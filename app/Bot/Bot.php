<?php

namespace App\Bot;

use pimax\FbBotApp;

class Bot
{
    public function __construct()
    {
    }
/*     [entry] => Array
    (
        [0] => Array
            (
                [id] => 298551551084975
                [time] => 1568520343736
                [messaging] => Array
                    (
                        [0] => Array
                            (
                                [sender] => Array
                                    (
                                        [id] => 2747936538563484
                                    )

                                [recipient] => Array
                                    (
                                        [id] => 298551551084975
                                    )

                                [timestamp] => 1568520343379
                                [message] => Array
                                    (
                                        [mid] => 6XJsF4JhJKq738SUkARpR4dz7qrNHqaqlPJjuKRFWO5Xgcr_3FUCaJkusdo-A_V9ypi82_ZcSDR8IIEgj8EraA
                                        [text] => Hi
                                    )

                            )

                    )

            )

    )
 */
    public function handleMessage(array $data)
    {
        if (!empty($data)) {
            $messaging = $data["messaging"][0];

            if (isset($messaging["postback"])) {
                if ($messaging["postback"]["payload"] == "get-started") {
                    return true;
                }
            }

            if (isset($messaging["message"])) {
                return $this->sendTextMessage($messaging["sender"]["id"], "မင်္ဂလာပါ မြန်မာယူနီကုဒ် ဧရိယာ အကူအညီမှ ကြိုဆိုပါသည်။");
            }
        }
    }

    private function sendTextMessage($recipientId, $messageText)
    {
        $messageData = [
            "recipient" => [
                "id" => $recipientId
            ],
            "message" => [
                "text" => $messageText
            ]
        ];
        $ch = curl_init('https://graph.facebook.com/v2.6/me/messages?access_token=' . env("PAGE_ACCESS_TOKEN"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($messageData));
        curl_exec($ch);
    }

    private function populateQuestion()
    {
        $options = ['computer', 'phone'];
    }
}
